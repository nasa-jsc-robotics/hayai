# hayai - the C++ benchmarking framework.

_hayai_ is a C++ framework for writing benchmarks for pieces of code along the lines of the normal approach for unit testing using frameworks such as [googletest](http://code.google.com/p/googletest/). For information on the origin of and how to use _hayai_, please read the [introductory blog post](https://bruun.co/2012/02/07/easy-cpp-benchmarking) until I have the time to write a proper README.

This repository is a [fork](https://github.com/nickbruun/hayai) that uses [catkin](http://wiki.ros.org/catkin) for package management, allowing other catkin packages to make use of the framework.